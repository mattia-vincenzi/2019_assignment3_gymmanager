# 2019_assignment3_GymManager

## Introduzione:
Progetto realizzato da:

- Mattia Vincenzi 860579
- Alessio Scheri 858579

Nel repository è presente un progetto Maven che possiede un insieme di classi Java e dialcuni relativi JUnit test. Anche se il team è composto da due persone, il progetto realizzato è un'applicazione Spring che implementa l'architettura MVC e la persistenza dei data con Java JPA. Questi contenuti si trovano all'interno della cartella **GymManager** del repository.

All'interno della cartella **docs** sono invece contenuti i file necessari alla creazione tramite *LaTeX* del file *Assignment3.pdf* realizzato per descrivere il progetto.

## Descrizione

Il progetto consiste in un semplice software per la gestione dei corsi di una palestra. Nello specifico è in grado di gestire le informazioni non solo dei corsi, ma anche dei clienti ad essi iscritti e degli insegnanti che li tengono.

L'applicazione dà la possibilità di creare, visualizzare e rimuovere nuovi corsi. I corsi sono descritti da un nome, un insegnante, un costo, una data di inizio e una data di fine. È importante sottolineare che in fase di progettazione è stato deciso che un corso può avere un solo insegnate, mentre un insegnate, naturalmente, può insegnare in più corsi. Non sono posti invece vincoli al cliente, che può iscriversi a più corsi.

Le stesse funzionalità sono messe a disposizione dei clienti e degli insegnanti. Siccome queste due entità hanno diverse caratteristiche in comune è presente anche una classe Persona della quale entrambi sono figli. La classe persona viene descritta da nome, cognome, indirizzo email, numero di telefono, data di nascita, indirizzo e città di residenza, ovvero da tutte quelle caratteristiche comuni ad entrambe le entità. A specializzare invece l'insegnante e il cliente sono rispettivamente la qualificazione e la cittadinanza.

L'ultimo elemento da sottolineare è il fatto che la palestra gestita dal sistema offre uno sconto del 20% in caso di iscrizione da parte di un figlio di un cliente già iscritto.

La descrizione completa del progetto è consultabile al file *Assignment3.pdf* presente all'interno della directory **docs**.

## Esecuzione

### Come eseguire i test

Per eseguire i test di integrazione, ovvero le classi predisposte per testare le operazioni CRUD su un database temporaneo:

`./mvn clean verify`

### Come creare il pacchetto jar

È possibile creare il jar dell'applicazione mediante il seguente comando:

`./mvnw clean package spring-boot:repackage`

Una volta creato il file jar, esso sarà disponibile al percorso: *target/GymManager-0.0.1-SNAPSHOT.jar*.

### Come avviare l'applicazione

Per la corretta esecuzione dell'applicazione è necessaria la presenza di [Docker Compose](https://docs.docker.com/compose/). Siccome l'applicazione si basa sulle informazioni contenute in un database, utilizzare questo strumento rende molto semplice la fase di configurazione iniziale ovunque la si voglia eseguire.

Dopo aver creato il pacchetto jar dell'applicazione, è necessario eseguire il processo di *build* con il comando:

`docker-compose build`

Dopodichè è possibile eseguire l'applicazione mediante il seguente comando:

`docker-compose up`

Una volta eseguita la web app essa sarà disponibile all'indirizzo [http://localhost:8080](http://localhost:8080) dove è presente una semplice pagina web che consente la navigazione.

### Popolazione iniziale del database

Il database, creato alla prima esecuzione dell'applicazione, sarà vuoto. All'interno del progetto è presente il file *src/main/resources/data.sql* che contiene al suo interno alcune semplici operazioni di insert per popolare inizialmente il database. Qualora lo si desiderasse è possibile eseguire queste query da un qualsiasi strumento desktop, oppure è possibile far si che venga eseguito questo file ogni volta che si lancia l'applicazione. Per fare questo è necessario andare nel file di properties *src/main/resources/application.properties* e rimuovere dal commento l'istruzione a riga 17 mostrata in *Figura 1*. 

![Figura 1 - Istruzioni per la popolazione iniziale.](GymManager/docs/imgs/instructions.png)
