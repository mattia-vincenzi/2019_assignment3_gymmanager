\section{Progetto}  %Title of the First Chapter

\subsection{Introduzione}

Il risultato dell'assignment è stato caricato su un repository GitLab e lo stato finale del lavoro è contenuto nel branch \textbf{master}. Esso è raggiungibile al seguente link:

\href{https://gitlab.com/mattia-vincenzi/2019_assignment3_gymmanager}{https://gitlab.com/mattia-vincenzi/2019\_assignment3\_gymmanager}


Nel repository è presente un progetto Maven composto da un insieme di classi Java e di alcuni relativi JUnit test. Anche se il team è composto da due persone, il progetto realizzato è un'applicazione Spring che implementa l'architettura MVC e la persistenza dei dati con Java JPA. Questi contenuti si trovano all'interno della cartella \textbf{GymManager} del repository.

All'interno della cartella \textbf{docs} sono invece presenti i file necessari alla creazione tramite \LaTeX di questo file pdf, realizzato per descrivere il progetto.

\subsection{Descrizione Applicazione}

Il progetto consiste in un semplice software per la gestione dei corsi di una palestra. Nello specifico è in grado di gestire le informazioni non solo dei corsi, ma anche dei clienti ad essi iscritti e degli insegnanti che li tengono.

L'applicazione dà la possibilità di creare, visualizzare e rimuovere nuovi corsi. I corsi sono descritti da un nome, un insegnante, un costo, una data di inizio e una data di fine. È importante sottolineare che in fase di progettazione è stato deciso che un corso può avere un solo insegnate, mentre un insegnate, naturalmente, può insegnare in più corsi. Non sono posti invece vincoli al cliente, che può iscriversi a più corsi.

Le stesse funzionalità sono messe a disposizione dei clienti e degli insegnanti. Siccome queste due entità hanno diverse caratteristiche in comune è presente anche una classe \textit{Persona} della quale entrambi sono figli. La classe persona viene descritta da nome, cognome, indirizzo email, numero di telefono, data di nascita, indirizzo e città di residenza, ovvero da tutte quelle caratteristiche comuni ad entrambe le entità. A specializzare invece l'insegnante e il cliente sono rispettivamente la qualificazione e la cittadinanza.

L'ultimo elemento da sottolineare è il fatto che la palestra gestita dal sistema offre uno sconto del 20\% in caso di iscrizione da parte di un figlio di un cliente già iscritto.

\subsection{Progettazione}

Il diagramma \textit{UML} (ER) in \textit{Figura 1} illustra graficamente le classi individuate per gestire la situazione descritta.

\begin{figure}[h!]
    \includegraphics[width=\linewidth]{imgs/UML.png}
    \centering
    \caption{\textit{Diagramma UML.}}
\end{figure}

La creazione del database non è stata effettuata direttamente da noi, ma viene effettuata da \textit{Hibernate}. Tuttavia, in fase di progettazione, con lo scopo di procedere con un'idea chiara del progetto, abbiamo creato il diagramma \textit{Entity-Relationship} (ER) riportato in \textit{Figura 2}.

\begin{figure}[h!]
    \includegraphics[width=0.8\linewidth]{imgs/Schema-ER.png}
    \centering
    \caption{\textit{Diagramma ER.}}
\end{figure}

Per avere poi chiara l'effettiva implementazione del database abbiamo utilizzato \textit{MySQL Workbench} per effettuare Reverse Engineer ed ottenere il diagramma \textit{Enhanced Entity Relationship} (EER) dello schema generato sul DB. \textit{Figura 3} mostra il risultato ottenuto.

\begin{figure}[h!]
    \includegraphics[width=0.8\linewidth]{imgs/Schema-EER.png}
    \centering
    \caption{\textit{Diagramma EER.}}
\end{figure}

\subsection{Organizzazione del progetto}

Abbiamo riunito classi, o più in generale entità, logicamente correlate all'interno di alcuni package:

\begin{itemize}
    \item \textbf{com.example.demo}: Questo package contiene l'entry point dell'applicazione.
    \item \textbf{com.example.demo.controller}: All'interno di questo package sono presenti i controller dell'applicazione. Ne è stato creato uno per ogni entità presente nella business logic dell'applicazione.
    \item \textbf{com.example.demo.exception}: All'interno di questo package sono presenti le classi rappresentanti le eccezioni che possono occorrere nell'interrogazione della base dati. 
    \item \textbf{com.example.demo.model}: All'interno di questo package sono presenti tutte le entità della business logic dell'applicazione, ovvero: \textit{Persona}, \textit{Cliente}, \textit{Insegnante} e \textit{Corso}.
    \item \textbf{com.example.demo.repository}: All'interno di questo package sono contenute le repository \textit{Spring Data} per l'implementazione delle query sul database. Avendo utilizzato {Spring Data} sono costituite solamente da interfacce i cui metodi hanno dei nomi specifici per le operazioni che il freamework deve automaticamente implementare.
    \item \textbf{com.example.demo.service}: All'interno di questo package sono state inserite le classe annotate come \textit{@Service}, ovvero classi che permettono di nascondere l'uso delle repository e di fornire dei servizi al controller. Rappresentano un layer intermedio che gestisce le chiamate del controller verso il model.
\end{itemize}

\subsection{Test}

All'interno del codice dell'applicativo sono stati inseriti anche diversi test d'integrazione per verificare l'effettivo funzionamento dello stesso. I test realizzati si possono trovare all'interno della cartella \textbf{src/test/java}.

Per i test è stata utilizzata la libreria   \textit{H2} che consente di sfruttare per le operazioni CRUD un database temporaneo mantenuto in memoria centrale. Una volta terminati i test il suo contenuto viene automaticamente eliminato, privandoci così degli oneri relativi alla gestione dei dati fittizi creati.

\begin{itemize}
    \item \textbf{ClientRepositoryIntegrationTest}: Questa classe consente di testare le query di inserimento, selezione ed eliminazione riguardanti la \textit{ClientRepository}.
    \item \textbf{CourseRepositoryIntegrationTest}: Questa classe consente di testare le query di inserimento, selezione ed eliminazione riguardanti la \textit{CourseRepository}.
    \item \textbf{TeacherRepositoryIntegrationTest}: Questa classe consente di testare le query di inserimento, selezione ed eliminazione riguardanti la \textit{TeacherRepository}.
\end{itemize}

\subsection{MVC}

\subsubsection{Model}

Il model della nostra applicazione, presente all'interno del package \newline \textbf{com.example.demo.model}, è costituito dalla semplice implementazione delle classi raffigurate nel diagramma di \textit{Figura 1}.
Le classi ed i rispettivi membri sono stati annotati con le annotazioni della \textit{Java Persistence API} (\textbf{JPA}), un framework che consente di gestire la persistenza dei dati di un database relazionale nelle applicazioni java.

\subsubsection{Controller}

Il \textit{DispatcherServlet} di Spring svolge il ruolo di front controller e delega le richieste dell'utente ai singoli controller. I controller realizzati nel progetto sono stati annotati con \textit{@Controller}, ovvero un'annotazione di Spring MVC che consente di interpretare le nostre classi come tali. All'interno di queste classi i metodi sono stati annotati per gestire richieste Get e Post dell'utente, al fine di reperire i dati necessari dal model e di passarli alla view successiva che deve essere mostrata all'utente.

\subsubsection{View}

Il componente View è implementato attraverso i file html conenuti nel percorso \textit{src/main/resources/templates} e sono coloro che consentono, effettivamente, di mostrare risultati e far interagire l'utente con la nostra applicazione. Per la loro realizzazione è stato utilizzato il template engine per Java chiamato \textit{Thymeleaf} che si integra perfettamente con il framework \textit{Spring} e semplifica notevolmente l'implementazione delle pagine web.

\subsection{Gestione eccezioni}

Le eccezioni sono presenti all'interno del package \newline \textbf{com.example.demo.exception} e sono relative alle entità del modello (ad esempio \textit{ClientNotFoundException} per \textit{Client} e così via).
Tali eccezioni vengono lanciate al mancato ritrovamento delle entità  nell'interrogazione della base di dati. Ogni qual volta una di queste eccezioni viene lanciata è accompagnata da un messaggio di errore che spiega il motivo per cui l'eccezione è stata sollevata. Le classi che rappresentano tali eccezioni sono marcate dall'annotazione \textit{@ResponseStatus}, che consente di specificare lo stato della risposta HTTP, e nel nostro caso il codice di ritorno scelto è stato 404 (\textit{Not Found}). In questo modo viene mostrata all'utente una classica pagina di errore con codice di risposta 404 e il messaggio di errore inserito al lancio dell'eccezione nell'applicazione. 
