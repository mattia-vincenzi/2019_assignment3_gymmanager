INSERT INTO person (id, name, surname, email, phone, address, city, birthday) VALUES
  (1, 'Antonio', 'Rossi', 'a.rossi@gmail.com', '3456789761', 'via Ronta 13', "Cesena", "1990-02-02");

INSERT INTO teacher (id, qualification) VALUES
  (1, "Istruttore Yoga");

INSERT INTO course (id, name, start_date, end_date, cost, teacher_id) VALUES
  (1, 'Yoga', '2020-02-02', '2021-02-02', 80.0, 1);

INSERT INTO person (id, name, surname, email, phone, address, city, birthday) VALUES
  (2, 'Giuseppe', 'Verdi', 'g.verdi@gmail.com', '1267895623', 'via Genova 44', "Mantova", "1997-11-22");

INSERT INTO client (id, citizenship) VALUES
  (2, "Italiana");

INSERT INTO enrollment (client_id, course_id) VALUES
  (2, 1);

INSERT INTO person (id, name, surname, email, phone, address, city, birthday) VALUES
  (3, 'Piero', 'Neri', 'p.neri@gmail.com', '3498624376', 'via Perugia 71', "Milano", "1995-07-11");

INSERT INTO client (id, citizenship, parent_id) VALUES
  (3, "Italiana", 2);

INSERT INTO enrollment (client_id, course_id) VALUES
  (3, 1);