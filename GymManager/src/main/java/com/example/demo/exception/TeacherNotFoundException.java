package com.example.demo.exception;

public class TeacherNotFoundException extends PersonNotFoundException {

	private static final long serialVersionUID = -1653336093708356589L;
	
	public TeacherNotFoundException() {
		super();
	}
	
	public TeacherNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public TeacherNotFoundException(String message) {
		super(message);
	}

	public TeacherNotFoundException(Throwable cause) {
		super(cause);
	}
}
