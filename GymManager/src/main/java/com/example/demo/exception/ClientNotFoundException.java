package com.example.demo.exception;

public class ClientNotFoundException extends PersonNotFoundException {

	private static final long serialVersionUID = 833552051554331556L;

	public ClientNotFoundException() {
		super();
	}
	
	public ClientNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public ClientNotFoundException(String message) {
		super(message);
	}

	public ClientNotFoundException(Throwable cause) {
		super(cause);
	}
}