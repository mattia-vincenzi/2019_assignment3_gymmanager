package com.example.demo.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.JoinColumn;

@Entity
public class Client extends Person {

	// Changed direction.
	// Store in a attribute list of child.
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id", nullable = true)
	private Client childOf;
	
	@OneToMany(mappedBy = "childOf", fetch = FetchType.LAZY)
	private List<Client> children;
	
	private String citizenship;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "enrollment", 
		joinColumns = { 
			@JoinColumn(name = "client_id", nullable = false)
		}, 
		inverseJoinColumns = {
			@JoinColumn(name = "course_id", nullable = false)
	})
	private List<Course> enrollments;

	public Client() { }
	
	public Client(String name, String surname, String email, String phone, String address, String city,
			Date birthday, Client childOf, String citizenship) {
		super(name, surname, email, phone, address, city, birthday);
		this.childOf = childOf;
		this.citizenship = citizenship;
		this.enrollments = new ArrayList<Course>();
		this.children = new ArrayList<Client>();
	}
    
	@PreRemove
	public void onPreRemove() {
		if (this.enrollments != null) {
			for (Course course : this.enrollments) {
				course.getClients().remove(this);
			}
		}
		if (this.children != null) {
			for (Client client : this.children) {
				client.setChildOf(null);
			}
		}
	}

	public Person getChildOf() {
		return childOf;
	}

	public void setChildOf(Client childOf) {
		this.childOf = childOf;
	}

	public String getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	public List<Course> getEnrollments() {
		return enrollments;
	}

	public void setEnrollments(List<Course> enrollments) {
		this.enrollments = enrollments;
	}

	public List<Client> getChildren() {
		return children;
	}

	public void setChildren(List<Client> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", name=" + name + ", surname=" + surname + ", email=" + email + ", phone=" + phone
				+ ", address=" + address + ", city=" + city + ", birthday=" + birthday + "childOf=" + childOf + ", citizenship=" + citizenship + ", enrollments=[?]]";
	}
}
