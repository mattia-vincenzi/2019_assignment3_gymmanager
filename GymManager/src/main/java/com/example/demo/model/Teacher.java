package com.example.demo.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;

@Entity
public class Teacher extends Person {
	private String qualification;

	// Managed by values is the name of the attribute annotated with name = "teacher".
	// Initially the name was "managedBy".
	// CascadeType persist means that only .save() operation are executed also for courses.
	@OneToMany(mappedBy = "teacher", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	private List<Course> courses;

	// Default Constructor.
	public Teacher() { }

	public Teacher(String name, String surname, String email, String phone, String address, String city,
			Date birthday, String qualification) {
		super(name, surname, email, phone, address, city, birthday);
		this.qualification = qualification;
		this.courses = new ArrayList<Course>();
	}
    
	@PreRemove
	public void onPreRemove() {
		if (this.courses != null) {
			for (Course course : this.courses) {
				course.setTeacher(null);
			}
		}
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

}