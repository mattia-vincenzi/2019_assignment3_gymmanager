package com.example.demo.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Course {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(value = TemporalType.DATE)
	private Date startDate;

	@Column(nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(value = TemporalType.DATE)
	private Date endDate;
	
	@Column(nullable = false)
	private float cost;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "teacher_id")
	private Teacher teacher;

	// CascadeType = Persist means that when we persist a course (with .save() or .persist())
	// we will automatically persist clients subscribed to this course.
	// It is useful because we can call only one save on a course to persist linked clients.
	@ManyToMany(mappedBy = "enrollments", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	private List<Client> clients;

	// Default constructor.
	public Course() { }

	public Course(String name, Date startDate, Date endDate, float cost, Teacher teacher) {
		super();
		this.name = name;
		this.startDate = startDate;
		this.endDate = endDate;
		this.cost = cost;
		this.setTeacher(teacher);
		this.clients = new ArrayList<Client>();
	}
    
	@PreRemove
	public void onPreRemove() {
		if (this.clients != null) {
			for (Client client : this.clients) {
				client.getEnrollments().remove(this);
			}
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Teacher getManagedBy() {
		return teacher;
	}

	public void setManagedBy(Teacher managedBy) {
		this.teacher = managedBy;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	
	public Teacher getTeacher () {
		return this.teacher;
	}

	@Override
	public String toString() {
		return "Course [id=" + id + ", name=" + name + ", startDate=" + startDate + ", endDate=" + endDate + ", cost="
				+ cost + ", teacher=" + teacher + ", clients=[?]]";
	}
}
