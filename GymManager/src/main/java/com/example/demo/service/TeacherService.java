package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.demo.model.Course;
import com.example.demo.model.Teacher;
import com.example.demo.repository.TeacherRepository;
import com.google.common.collect.Lists;

@Service
public class TeacherService {

	private TeacherRepository teacherRepository;
	
	public TeacherService(TeacherRepository teacherRepository) {
		this.teacherRepository = teacherRepository;
	}
	
	public List<Teacher> getTeachers() {
		return Lists.newArrayList(teacherRepository.findAll());
	}
	
	public Optional<Teacher> getTeacher(Long id) {
		return teacherRepository.findById(id);
	}
	
	public List<Teacher> getByNameAndSurname(String name, String surname) {
		return teacherRepository.findByNameAndSurnameAllIgnoreCase(name, surname);
	}
	
	public List<Teacher> getByQualification(String qualification) {
		return teacherRepository.findByQualification(qualification);
	}
	
	public Optional<Teacher> getByCourse(Course course) {
		return teacherRepository.findByCourses(course);
	}
	
	public void deleteTeacher(Teacher teacher) {
		teacherRepository.delete(teacher);
	}
	
	public Teacher insertOrUpdateTeacher(Teacher teacher) {
		return teacherRepository.save(teacher);
	}
}
