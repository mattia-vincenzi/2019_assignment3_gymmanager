package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.demo.model.Client;
import com.example.demo.model.Course;
import com.example.demo.repository.ClientRepository;
import com.google.common.collect.Lists;

@Service
public class ClientService {
	
	private ClientRepository clientRepository;
	
	public ClientService(ClientRepository clientRepository) {
		this.clientRepository = clientRepository;
	}
	
	public List<Client> getClients(){
		return Lists.newArrayList(clientRepository.findAll());
	}
	
	public Optional<Client> getClient(Long id) {
		return clientRepository.findById(id); 
	}
	
	public List<Client> getClientsByNameAndSurname(String name, String surname) {
		return clientRepository.findByNameAndSurnameAllIgnoreCase(name, surname);
	}
	
	public List<Client> getByEnrollment(Course course) {
		return clientRepository.findByEnrollments(course);
	}
	
	public List<Client> getByParent(Client parent) {
		return clientRepository.findByChildOf(parent);
	}
	
	public void deleteClient(Client client) {
		clientRepository.delete(client);
	}
	
	public Client insertOrUpdateClient(Client client) {
		return clientRepository.save(client);
	}
}
