package com.example.demo.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.demo.model.Client;
import com.example.demo.model.Course;
import com.example.demo.model.Teacher;
import com.example.demo.repository.CourseRepository;
import com.google.common.collect.Lists;

@Service
public class CourseService {

	private CourseRepository courseRepository;
	
	public CourseService(CourseRepository courseRepository) {
		this.courseRepository = courseRepository;
	}
	
	public List<Course> getCourses() {
		return Lists.newArrayList(courseRepository.findAll());
	}
	
	public Optional<Course> getCourse(Long id) {
		return courseRepository.findById(id);
	}
	
	public List<Course> getByName(String name) {
		return courseRepository.findByNameIgnoreCase(name);
	}
	
	public List<Course> getByInfixInName(String infix) {
		return courseRepository.findByNameContaining(infix);
	}
	
	public List<Course> getByCourseWithCostLessThan(float cost) {
		return courseRepository.findByCostLessThanEqualOrderByCostAsc(cost);
	}
	
	public List<Course> getByCourseWithCostGreaterThan(float cost) {
		return courseRepository.findByCostGreaterThanEqualOrderByCostAsc(cost);
	}
	
	public List<Course> getByCourseWithCostBetween(float lower, float upper) {
		return courseRepository.findByCostBetween(lower, upper);
	}
	
	public List<Course> getByClient(Client client) {
		return courseRepository.findByClients(client);
	}
	
	public List<Course> getByTeacher(Teacher teacher) {
		return courseRepository.findByTeacher(teacher);
	}
	
	public List<Course> getCoursesBeforeStartDate(Date date) {
		return courseRepository.findByStartDateBefore(date);
	}
	
	public List<Course> getCoursesAfterEndDate(Date date) {
		return courseRepository.findByEndDateAfter(date);
	}
	
	public void deleteCourse(Course course) {
		courseRepository.delete(course);
	}
	
	public Course insertOrUpdateCourse(Course course) {
		return courseRepository.save(course);
	}
}
