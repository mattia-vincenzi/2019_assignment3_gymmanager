package com.example.demo.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.exception.ClientNotFoundException;
import com.example.demo.exception.CourseNotFoundException;
import com.example.demo.model.Client;
import com.example.demo.model.Course;
import com.example.demo.service.ClientService;
import com.example.demo.service.CourseService;

@Controller
public class ClientController {

	private ClientService clientService;
	private CourseService courseService;

	// Note: If you're on Spring 4.3+ and your target class has just one
	// constructor, you can omit autowired annotation. Spring will inject all the
	// needed dependencies for it.
	public ClientController(ClientService clientService, CourseService courseService) {
		this.clientService = clientService;
		this.courseService = courseService;
	}

	// --------------------------------------------------------------------
	// Mapping of find requests.
	// --------------------------------------------------------------------
	@GetMapping("/clients")
	public ModelAndView clients() {
		ModelAndView modelAndView = new ModelAndView();

		List<Client> clients = clientService.getClients();

		modelAndView.addObject("clients", clients);

		modelAndView.setViewName("clients");
		
		if (!clients.isEmpty())
			modelAndView.addObject("client_costs", getClientsCosts(clients));

		return modelAndView;
	}

	@GetMapping("/client_by_id/{id}")
	public ModelAndView clientById(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView();
		List<Client> clients = new ArrayList<>();

		if (id > 0) {
			Optional<Client> client = clientService.getClient(id);
			client.orElseThrow(() -> new ClientNotFoundException("Client whith id " + id + " not found."));
			clients.add(client.get());
		} else {
			throw new ClientNotFoundException("Client not found: id < 0.");
		}
		
		modelAndView.addObject("clients", clients);
		modelAndView.addObject("client_costs", getClientsCosts(clients));
		
		modelAndView.setViewName("clients");

		return modelAndView;
	}
	
	@GetMapping("/clients_by_name/{name}/{surname}")
	public ModelAndView clientsByNameAndSurname(@PathVariable("name") String name, @PathVariable("surname") String surname) {
		ModelAndView modelAndView = new ModelAndView();

		if (name.equals("-")) {
			name = "";
		}
		if (surname.equals("-")) {
			surname = "";
		}

		List<Client> clients = clientService.getClientsByNameAndSurname(name, surname);
		
		if (clients.isEmpty())
			throw new ClientNotFoundException("Client \"" + name.toUpperCase() + " " + surname.toUpperCase() + "\" not found.");

		modelAndView.addObject("clients", clients);
		modelAndView.addObject("client_costs", getClientsCosts(clients));

		modelAndView.setViewName("clients");

		return modelAndView;
	}
	
	@GetMapping("/clients_by_course/{course_id}")
	public ModelAndView clientsByEnrollments(@PathVariable("course_id") Long id) {
		ModelAndView modelAndView = new ModelAndView();
		List<Client> clients = new ArrayList<>();

		if (id > 0) {
			Optional<Course> course = courseService.getCourse(id);
			course.orElseThrow(() -> new CourseNotFoundException("Course with id: " + id + " not found."));
			
			clients.addAll(clientService.getByEnrollment(course.get()));
			if (clients.isEmpty())
				throw new ClientNotFoundException("There are no clients that attend: " + course.get().getName() + ".");
		} else {
			throw new CourseNotFoundException("Course not found: id < 0.");
		}

		modelAndView.addObject("clients", clients);
		modelAndView.addObject("client_costs", getClientsCosts(clients));

		modelAndView.setViewName("clients");

		return modelAndView;
	}
	
	@PostMapping("/delete_client")
	public ModelAndView deleteClient(@RequestParam Long id, RedirectAttributes redirectAttributes) {
		Client client = clientService.getClient(id).get();
		
		clientService.deleteClient(client);

		ModelAndView modelAndView = new ModelAndView("redirect:/clients");

		return modelAndView;
	}
	
	// --------------------------------------------------------------------
	// Mapping of insert/update requests.
	// --------------------------------------------------------------------
	@PostMapping("/save_client")
	public ModelAndView saveClient(@ModelAttribute Client client) {
		clientService.insertOrUpdateClient(client);

		ModelAndView modelAndView = new ModelAndView("redirect:/clients");

		return modelAndView;
	}

	@RequestMapping(value = "/insert_or_update_client", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView insertOrUpdateClient(@RequestParam(required = false) Long id) {
		ModelAndView modelAndView = new ModelAndView();
		Client client;
		if (id != null) {
			client = clientService.getClient(id).get();
		}
		else {
			client = new Client();
		}
		
		List<Course> otherCourses = courseService.getCourses();
		
		if (client.getEnrollments() != null) {
			otherCourses.removeAll(client.getEnrollments());
		}

		List<Client> parents = clientService.getClients();
		parents.remove(client);
		Iterator<Client> iter = parents.listIterator();
		while(iter.hasNext()){
			Client parent = iter.next();
			if (parent.getChildOf() != null && parent.getChildOf().equals(client)) {
		        iter.remove();
		    }
		}

		modelAndView.addObject("parents", parents);		
		modelAndView.addObject("client", client);
		modelAndView.addObject("other_courses", otherCourses);
		
		modelAndView.setViewName("insert_or_update_client");

		return modelAndView;
	}
	
	private List<List<?>> getClientsCosts(List<Client> clients) {
		List<List<?>> clientCosts = new ArrayList<List<?>> ();
		
		if (clients.isEmpty())
			throw new ClientNotFoundException();
		
		for (Client c : clients) {
			ArrayList<Float>costs = new ArrayList<>();
			if (c.getEnrollments() != null) {
				for (Course course: c.getEnrollments()) {
					if (c.getChildOf() != null) {
						costs.add((float)(course.getCost() - course.getCost() * 0.2));
					}
					else {
						costs.add(course.getCost());
					}
				}
			}
			clientCosts.add(costs);
		}
		
		return clientCosts;
	}
}
