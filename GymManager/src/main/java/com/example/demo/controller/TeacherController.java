package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.exception.CourseNotFoundException;
import com.example.demo.exception.TeacherNotFoundException;
import com.example.demo.model.Course;
import com.example.demo.model.Teacher;
import com.example.demo.service.CourseService;
import com.example.demo.service.TeacherService;

@Controller
public class TeacherController {
	
	private TeacherService teacherService;
	private CourseService courseService;

	// Note: If you're on Spring 4.3+ and your target class has just one
	// constructor, you can omit autowired annotation. Spring will inject all the
	// needed dependencies for it.
	public TeacherController(TeacherService teacherService, CourseService courseService) {
		this.teacherService = teacherService;
		this.courseService = courseService;
	}
	
	// --------------------------------------------------------------------
	// Mapping of find requests.
	// --------------------------------------------------------------------
	@GetMapping("/teachers")
	public ModelAndView teachers() {
		ModelAndView modelAndView = new ModelAndView();

		List<Teacher> teachers = teacherService.getTeachers();

		modelAndView.addObject("teachers", teachers);

		modelAndView.setViewName("teachers");

		return modelAndView;
	}
	
	@GetMapping("/teacher_by_id/{id}")
	public ModelAndView teachersById(@PathVariable(name="id") Long id) {
		ModelAndView modelAndView = new ModelAndView();
		List<Teacher> teachers = new ArrayList<>();

		if (id > 0) {
			Optional<Teacher> teacher = teacherService.getTeacher(id);
			teacher.orElseThrow(() -> new TeacherNotFoundException("Teaher with id: " + id + " not found."));
			teachers.add(teacher.get());
		} else {
			throw new TeacherNotFoundException("Teacher not found: id < 0."); 
		}

		modelAndView.addObject("teachers", teachers);

		modelAndView.setViewName("teachers");

		return modelAndView;
	}
	
	@GetMapping("/teachers_by_name/{name}/{surname}")
	public ModelAndView teachersByNameAndSurname(@PathVariable(name="name") String name, @PathVariable(name="surname") String surname) {
		ModelAndView modelAndView = new ModelAndView();

		if (name.equals("-")) {
			name = "";
		}
		if (surname.equals("-")) {
			surname = "";
		}

		List<Teacher> teachers = teacherService.getByNameAndSurname(name, surname);
		
		if (teachers.isEmpty())
			throw new TeacherNotFoundException("Teacher \"" + name.toUpperCase() + " " + surname.toUpperCase() + "\" not found."); 

		modelAndView.addObject("teachers", teachers);

		modelAndView.setViewName("teachers");

		return modelAndView;
	}
	
	@GetMapping("/teachers_by_qualification/{qualification}")
	public ModelAndView teachersByQualification(@PathVariable(name="qualification") String qualification) {
		ModelAndView modelAndView = new ModelAndView();

		if (qualification.equals("-")) {
			qualification= "";
		}

		List<Teacher> teachers = teacherService.getByQualification(qualification);
		
		if (teachers.isEmpty())
			throw new TeacherNotFoundException("Teachers with qualification " + qualification + " not found."); 

		modelAndView.addObject("teachers", teachers);

		modelAndView.setViewName("teachers");

		return modelAndView;
	}
	
	@GetMapping("/teachers_by_course/{course_id}")
	public ModelAndView teachersByCourse(@PathVariable(name="course_id") Long id) {
		ModelAndView modelAndView = new ModelAndView();
		List<Teacher> teachers = new ArrayList<>();

		if (id > 0) {
			Optional<Course> course = courseService.getCourse(id);
			course.orElseThrow(() -> new CourseNotFoundException("Course with id: " + id + " not found."));
			
			Optional<Teacher> teacher = teacherService.getByCourse(course.get());
			teacher.orElseThrow(() -> new TeacherNotFoundException("There is no any teacher that teachs in course with id: " + id + "."));
			teachers.add(teacher.get());
		} else {
			throw new CourseNotFoundException("Course not found: id < 0."); 
		}

		modelAndView.addObject("teachers", teachers);
		
		modelAndView.setViewName("teachers");

		return modelAndView;
	}
	
	@PostMapping("/delete_teacher")
	public ModelAndView deleteTeacher(@RequestParam Long id, RedirectAttributes redirectAttributes) {
		Teacher teacher = teacherService.getTeacher(id).get();
		
		teacherService.deleteTeacher(teacher);

		ModelAndView modelAndView = new ModelAndView("redirect:/teachers");

		return modelAndView;
	}
	
	// --------------------------------------------------------------------
	// Mapping of insert/update requests.
	// --------------------------------------------------------------------
	@PostMapping("/save_teacher")
	public ModelAndView saveTeacher(@ModelAttribute Teacher teacher) {
		teacherService.insertOrUpdateTeacher(teacher);

		ModelAndView modelAndView = new ModelAndView("redirect:/teachers");

		return modelAndView;
	}
	
	@RequestMapping(value = "/insert_or_update_teacher", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView insertOrUpdateTeacher(@RequestParam(required = false) Long id) {
		ModelAndView modelAndView = new ModelAndView();
		Teacher teacher;
		if (id != null) {
			teacher = teacherService.getTeacher(id).get();
		}
		else {
			teacher = new Teacher();
		}
		
		modelAndView.addObject("teacher", teacher);
		
		modelAndView.setViewName("insert_or_update_teacher");

		return modelAndView;
	}
}
