package com.example.demo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.exception.ClientNotFoundException;
import com.example.demo.exception.CourseNotFoundException;
import com.example.demo.exception.TeacherNotFoundException;
import com.example.demo.model.Client;
import com.example.demo.model.Course;
import com.example.demo.model.Teacher;
import com.example.demo.service.ClientService;
import com.example.demo.service.CourseService;
import com.example.demo.service.TeacherService;

@Controller
public class CourseController {
	
	private CourseService courseService;
	private ClientService clientService;
	private TeacherService teacherService;

	// Note: If you're on Spring 4.3+ and your target class has just one
	// constructor, you can omit autowired annotation. Spring will inject all the
	// needed dependencies for it.
	public CourseController(CourseService courseService, ClientService clientService, TeacherService teacherService) {
		this.courseService = courseService;
		this.clientService = clientService;
		this.teacherService = teacherService;
	}
	
	// --------------------------------------------------------------------
	// Mapping of find requests.
	// --------------------------------------------------------------------
	@GetMapping("/courses")
	public ModelAndView courses() {
		ModelAndView modelAndView = new ModelAndView();

		List<Course> courses = courseService.getCourses();

		modelAndView.addObject("courses", courses);

		modelAndView.setViewName("courses");

		return modelAndView;
	}
	
	@GetMapping("/course_by_id/{id}")
	public ModelAndView coursesById(@PathVariable(name="id") Long id) {
		ModelAndView modelAndView = new ModelAndView();
		List<Course> courses = new ArrayList<>();

		if (id > 0) {
			Optional<Course> course = courseService.getCourse(id);
			course.orElseThrow(() -> new CourseNotFoundException("Course with id: " + id + " not found."));
			courses.add(course.get());
		} else {
			throw new CourseNotFoundException("Course not found: id < 0.");
		}
		
		modelAndView.addObject("courses", courses);

		modelAndView.setViewName("courses");

		return modelAndView;
	}	
	
	@GetMapping("/courses_by_name/{name}")
	public ModelAndView coursesByName(@PathVariable(name="name") String name) {
		ModelAndView modelAndView = new ModelAndView();

		if (name.equals("-")) {
			name = "";
		}

		List<Course> courses = courseService.getByName(name);
		
		if (courses.isEmpty())
			throw new CourseNotFoundException("Corse with name \"" + name.toUpperCase() + "\" not found."); 
		
		modelAndView.addObject("courses", courses);

		modelAndView.setViewName("courses");

		return modelAndView;
	}
	
	@GetMapping("/courses_by_infix/{infix}")
	public ModelAndView coursesByNameContaining(@PathVariable(name="infix") String infix) {
		ModelAndView modelAndView = new ModelAndView();

		if (infix.equals("-")) {
			infix = "";
		}

		List<Course> courses = courseService.getByInfixInName(infix);
		
		if (courses.isEmpty())
			throw new CourseNotFoundException("Courses with substring \"" + infix + "\" in name, not found."); 
		
		modelAndView.addObject("courses", courses);

		modelAndView.setViewName("courses");

		return modelAndView;
	}
	
	@GetMapping("/courses_by_cost_greater/{cost_greater}")
	public ModelAndView coursesByCostGreater(@PathVariable(name="cost_greater") float cost) {
		ModelAndView modelAndView = new ModelAndView();
		List<Course> courses = new ArrayList<>();

		if (cost > 0) {
			courses = courseService.getByCourseWithCostGreaterThan(cost);
			if (courses.isEmpty())
				throw new CourseNotFoundException("Courses with cost greater than: " + cost + " not found."); 
		} else {
			throw new CourseNotFoundException("Course not found: cost < 0."); 
		}
		
		modelAndView.addObject("courses", courses);

		modelAndView.setViewName("courses");

		return modelAndView;
	}
	
	@GetMapping("/courses_by_cost_less/{cost_less}")
	public ModelAndView coursesByCostLess(@PathVariable(name="cost_less") float cost) {
		ModelAndView modelAndView = new ModelAndView();
		List<Course> courses = new ArrayList<>();

		if (cost > 0) {
			courses = courseService.getByCourseWithCostLessThan(cost);
			if (courses.isEmpty())
				throw new CourseNotFoundException("Courses with cost less than: " + cost + " not found.");  
		} else {
			throw new CourseNotFoundException("Course not found: cost < 0."); 
		}
		
		modelAndView.addObject("courses", courses);

		modelAndView.setViewName("courses");

		return modelAndView;
	}
	
	@GetMapping("/courses_by_cost_between/{cost_lower}/{cost_upper}")
	public ModelAndView coursesByCostBetween(@PathVariable(name="cost_lower") float lower, @PathVariable(name="cost_upper") float upper) {
		ModelAndView modelAndView = new ModelAndView();
		List<Course> courses = new ArrayList<>();

		if (lower > 0 && upper > 0 && upper >= lower) {
			courses = courseService.getByCourseWithCostBetween(lower, upper);
			if (courses.isEmpty())
				throw new CourseNotFoundException("Courses with cost between [" + lower + "," + upper + "] not found."); 
		} else {
			throw new CourseNotFoundException("Course not found: one of costs < 0."); 
		}
		
		modelAndView.addObject("courses", courses);

		modelAndView.setViewName("courses");

		return modelAndView;
	}
	
	@GetMapping("/courses_by_client/{client_id}")
	public ModelAndView coursesByClient(@PathVariable(name="client_id") Long clientId) {
		ModelAndView modelAndView = new ModelAndView();
		List<Course> courses = new ArrayList<>();

		if (clientId > 0) {
			Optional<Client> client = clientService.getClient(clientId);
			client.orElseThrow(() -> new ClientNotFoundException("Client with id: " + clientId + " not found."));
			
			courses = courseService.getByClient(client.get());
			if (courses.isEmpty())
				throw new CourseNotFoundException("Client with id: " + clientId + " does not attend any courses."); 
		} else {
			throw new ClientNotFoundException("Client not found: id < 0.");
		}
		
		modelAndView.addObject("courses", courses);

		modelAndView.setViewName("courses");

		return modelAndView;
	}
	
	@GetMapping("/courses_by_teacher/{teacher_id}")
	public ModelAndView coursesByTeacher(@PathVariable(name="teacher_id") Long teacherId) {
		ModelAndView modelAndView = new ModelAndView();
		List<Course> courses = new ArrayList<>();

		if (teacherId > 0) {
			Optional<Teacher> teacher = teacherService.getTeacher(teacherId);
			teacher.orElseThrow(() -> new TeacherNotFoundException("Teacher with id: " + teacherId + " not found"));
			
			courses = courseService.getByTeacher(teacher.get());
			if (courses.isEmpty())
				throw new CourseNotFoundException("There are no courses where teacher with id: " + teacherId + " teaches."); 
		} else {
			throw new TeacherNotFoundException("Teacher not found: id < 0.");
		}
		
		modelAndView.addObject("courses", courses);

		modelAndView.setViewName("courses");

		return modelAndView;
	}
	
	@GetMapping("/courses_by_startdate/{start_date}")
	public ModelAndView coursesByStartDateBefore(@PathVariable(name="start_date") String before) {
		ModelAndView modelAndView = new ModelAndView();
		List<Course> courses = new ArrayList<>();
		
		try {
			courses = courseService.getCoursesBeforeStartDate(new SimpleDateFormat("yyyy-MM-dd").parse(before));
			if (courses.isEmpty())
				throw new CourseNotFoundException("Courses with start date before " + before + " not found."); 
		} catch (ParseException e) {
			;
		}
		
		modelAndView.addObject("courses", courses);

		modelAndView.setViewName("courses");

		return modelAndView;
	}
	
	@GetMapping("/courses_by_enddate/{end_date}")
	public ModelAndView coursesByEndDateAfter(@PathVariable(name="end_date") String after) {
		ModelAndView modelAndView = new ModelAndView();
		List<Course> courses = new ArrayList<>();
		
		try {
			courses = courseService.getCoursesAfterEndDate(new SimpleDateFormat("yyyy-MM-dd").parse(after));
			if (courses.isEmpty())
				throw new CourseNotFoundException("Courses with end date after " + after + " not found."); 
		} catch (ParseException e) {
			;
		}
		
		modelAndView.addObject("courses", courses);

		modelAndView.setViewName("courses");

		return modelAndView;
	}
	
	@PostMapping("/delete_course")
	public ModelAndView deleteCourse(@RequestParam Long id, RedirectAttributes redirectAttributes) {
		Course course = courseService.getCourse(id).get();
		
		courseService.deleteCourse(course);

		ModelAndView modelAndView = new ModelAndView("redirect:/courses");

		return modelAndView;
	}
	
	// --------------------------------------------------------------------
	// Mapping of insert/update requests.
	// --------------------------------------------------------------------
	@PostMapping("/save_course")
	public ModelAndView saveTeacher(@ModelAttribute Course course) {
		courseService.insertOrUpdateCourse(course);

		ModelAndView modelAndView = new ModelAndView("redirect:/courses");

		return modelAndView;
	}
	
	@RequestMapping(value = "/insert_or_update_course", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView insertOrUpdateCourse(@RequestParam(required = false) Long id) {
		ModelAndView modelAndView = new ModelAndView();
		Course course;
		if (id != null) {
			course = courseService.getCourse(id).get();
		}
		else {
			course = new Course();
		}
		
		List <Teacher> teachers = teacherService.getTeachers();
		System.out.println(teachers);
		if (course.getTeacher() != null) {
			teachers.remove(course.getTeacher());
		}
		
		modelAndView.addObject("course", course);
		modelAndView.addObject("teachers", teachers);
		
		modelAndView.setViewName("insert_or_update_course");

		return modelAndView;
	}
}
