package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.example.demo.model.Course;
import com.example.demo.model.Teacher;

@Repository
public interface TeacherRepository extends PersonRepository<Teacher> {

	List<Teacher> findByQualification(String qualification);
	
	// One teacher can teach in many courses but one course has only one teacher.
	Optional<Teacher> findByCourses(Course course);
}
