package com.example.demo.repository;

import java.util.Date;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.example.demo.model.Client;
import com.example.demo.model.Course;
import com.example.demo.model.Teacher;

public interface CourseRepository extends CrudRepository<Course, Long> {
	
	List<Course> findByTeacher(Teacher teacher);
	
	List<Course> findByStartDateBefore(Date before);
	
	List<Course> findByEndDateAfter(Date after);
	
	List<Course> findByNameIgnoreCase(String name);
	
	List<Course> findByClients(Client c);
	
	List<Course> findByNameContaining(String infix);
	
	List<Course> findByCostLessThanEqualOrderByCostAsc(float cost);
	
	List<Course> findByCostGreaterThanEqualOrderByCostAsc(float cost);
	
	List<Course> findByCostBetween(float lower_bound, float upper_bound);
}
