package com.example.demo.repository;

import java.util.List;

import com.example.demo.model.Client;
import com.example.demo.model.Course;
import com.example.demo.model.Person;

public interface ClientRepository extends PersonRepository<Client> {

	List<Client> findByCitizenship(String citizenship);
	
	List<Client> findByChildOf(Person childOf);
	
	List<Client> findByChildOfIsNull();
	
	List<Client> findByChildOfIsNotNull();
	
	List<Client> findByEnrollments(Course c);
	
	List<Client> findByEnrollmentsIsNull();
	
	List<Client> findByEnrollmentsIsNotNull();
}
