package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.example.demo.model.Person;

@NoRepositoryBean
public interface PersonRepository<T extends Person> extends CrudRepository<T, Long> {
	
	// To see all person that attending the gym specifying birthday.
	List<T> findByBirthdayBefore(Date before);
	
	List<T> findByBirthdayAfter(Date after);
	
	List<T> findByBirthdayBetween(Date start, Date end);
	
	List<T> findByNameAndSurnameAllIgnoreCase(String name, String surname);
}
