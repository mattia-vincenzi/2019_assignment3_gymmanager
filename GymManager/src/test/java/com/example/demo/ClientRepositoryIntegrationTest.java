package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.model.Client;
import com.example.demo.model.Course;
import com.example.demo.repository.ClientRepository;
import com.google.common.collect.Lists;

@RunWith(SpringRunner.class)
@TestPropertySource(locations="classpath:test.properties")
@DataJpaTest
public class ClientRepositoryIntegrationTest {
 
    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private ClientRepository clientRepository;
 
    @Test
    public void whenFindByCitizenship() {
    	
		try {
			Client client1 = new Client("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()).getTime()), null, "Italiana");
	        Client client2 = new Client("Giuseppe", "Verdi", "giuseppe.verdi@mail.com", "+396666666666", "Via dei mille, 2", "Modena", new Date(new Date(new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01").getTime()).getTime()), null, "Albanese");
	        entityManager.persist(client1);
	        entityManager.persist(client2);
	        entityManager.flush();

	        // when
	        List<Client> founds = clientRepository.findByCitizenship(client1.getCitizenship());
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getCitizenship()).isEqualTo(client1.getCitizenship());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByChildOf() {
    	
		try {
	        Client client2 = new Client("Giuseppe", "Verdi", "giuseppe.verdi@mail.com", "+396666666666", "Via dei mille, 2", "Modena", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01").getTime()), null, "Albanese");
			Client client1 = new Client("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()).getTime()), client2, "Italiana");
	        entityManager.persist(client1);
	        entityManager.persist(client2);
	        entityManager.flush();

	        // when
	        List<Client> founds = clientRepository.findByChildOf(client2);
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0)).isEqualTo(client1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByChildOfIsNull() {
    	
		try {
	        Client client1 = new Client("Giuseppe", "Verdi", "giuseppe.verdi@mail.com", "+396666666666", "Via dei mille, 2", "Modena", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01").getTime()), null, "Albanese");
	        entityManager.persist(client1);
	        entityManager.flush();

	        // when
	        List<Client> founds = clientRepository.findByChildOfIsNull();
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0)).isEqualTo(client1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByChildOfIsNotNull() {
    	
		try {
	        Client client2 = new Client("Giuseppe", "Verdi", "giuseppe.verdi@mail.com", "+396666666666", "Via dei mille, 2", "Modena", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01").getTime()), null, "Albanese");
			Client client1 = new Client("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), client2, "Italiana");
	        entityManager.persist(client1);
	        entityManager.persist(client2);
	        entityManager.flush();

	        // when
	        List<Client> founds = clientRepository.findByChildOfIsNotNull();
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0)).isEqualTo(client1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByEnrollments() {
    	
		try {
			Course course = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, null);
			entityManager.persist(course);
			Client client1 = new Client("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), null, "Italiana");
	        client1.getEnrollments().add(course);
	        entityManager.persist(client1);
	        entityManager.flush();

	        // when
	        List<Client> founds = clientRepository.findByEnrollments(course);
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0)).isEqualTo(client1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByEnrollmentsIsNull() {
    	
		try {
			Client client1 = new Client("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), null, "Italiana");
	        entityManager.persist(client1);
	        entityManager.flush();

	        // when
	        List<Client> founds = clientRepository.findByEnrollmentsIsNull();
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0)).isEqualTo(client1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByEnrollmentsIsNotNull() {
    	
		try {
			Course course = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, null);
			entityManager.persist(course);
			Client client1 = new Client("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), null, "Italiana");
	        client1.getEnrollments().add(course);
	        entityManager.persist(client1);
	        entityManager.flush();

	        // when
	        List<Client> founds = clientRepository.findByEnrollmentsIsNotNull();
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0)).isEqualTo(client1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByBirthdayBefore() {
    	
		try {
			Client client1 = new Client("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), null, "Italiana");
	        entityManager.persist(client1);
	        entityManager.flush();

	        // when
	        List<Client> founds = clientRepository.findByBirthdayBefore(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-02"));
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getBirthday()).isEqualTo(client1.getBirthday());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByBirthdayAfter() {

		try {
			Client client1 = new Client("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), null, "Italiana");
	        entityManager.persist(client1);
	        entityManager.flush();

	        // when
	        List<Client> founds = clientRepository.findByBirthdayAfter(new SimpleDateFormat("yyyy-MM-dd").parse("1969-12-31"));
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getBirthday()).isEqualTo(client1.getBirthday());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByBirthdayBetween() {
    	
		try {
			Client client1 = new Client("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), null, "Italiana");
	        entityManager.persist(client1);
	        entityManager.flush();

	        // when
	        List<Client> founds = clientRepository.findByBirthdayBetween(new SimpleDateFormat("yyyy-MM-dd").parse("1969-12-31"), new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-02"));
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getBirthday()).isEqualTo(client1.getBirthday());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByNameAndSurnameIgnoreCase() {
    	try {
			Client client1 = new Client("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), null, "Italiana");
	        entityManager.persist(client1);
	        entityManager.flush();

	        // when
	        List<Client> founds = clientRepository.findByNameAndSurnameAllIgnoreCase(client1.getName().toUpperCase(), client1.getSurname().toUpperCase());
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getName()).isEqualTo(client1.getName());
	        assertThat(founds.get(0).getSurname()).isEqualTo(client1.getSurname());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenDeleteCourse() {
    	try {
			Course course = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, null);
			Client client1 = new Client("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), null, "Italiana");
	        client1.getEnrollments().add(course);
			entityManager.persist(course);
	        entityManager.persist(client1);
	        entityManager.flush();
	        entityManager.refresh(course);

	        // when
	        List<Client> founds = clientRepository.findByEnrollments(course);
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0)).isEqualTo(client1);

	        entityManager.remove(course);
	        entityManager.flush();

	        // when
	        founds = Lists.newArrayList(clientRepository.findAll());
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0)).isEqualTo(client1);

	        // when
	        founds = clientRepository.findByEnrollments(course);
	     
	        // then
	        assertThat(founds.size()).isZero();
	        
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
}