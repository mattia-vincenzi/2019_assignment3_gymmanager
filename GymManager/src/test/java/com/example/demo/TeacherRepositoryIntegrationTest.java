package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.model.Course;
import com.example.demo.model.Teacher;
import com.example.demo.repository.TeacherRepository;
import com.google.common.collect.Lists;

@RunWith(SpringRunner.class)
@TestPropertySource(locations="classpath:test.properties")
@DataJpaTest
public class TeacherRepositoryIntegrationTest {
 
    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private TeacherRepository teacherRepository;
 
    @Test
    public void whenFindByQualification() {
    	
		try {
			Teacher teacher1 = new Teacher("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), "Laurea triennale in scienze motorie");
	        entityManager.persist(teacher1);
	        entityManager.flush();

	        // when
	        List<Teacher> founds = teacherRepository.findByQualification(teacher1.getQualification());
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getQualification()).isEqualTo(teacher1.getQualification());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByCourses() {
    	
		try {
			Teacher teacher1 = new Teacher("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), "Laurea triennale in scienze motorie");
	        entityManager.persist(teacher1);
			Course course = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, teacher1);
			entityManager.persist(course);
	        entityManager.flush();

	        // when
	        Optional<Teacher> found = teacherRepository.findByCourses(course);
	     
	        // then
	        assertThat(found.isPresent());
	        assertThat(found.equals(teacher1));
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByBirthdayBefore() {
    	
		try {
			Teacher teacher1 = new Teacher("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), "Laurea triennale in scienze motorie");
	        entityManager.persist(teacher1);
	        entityManager.flush();

	        // when
	        List<Teacher> founds = teacherRepository.findByBirthdayBefore(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-02"));
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getBirthday()).isEqualTo(teacher1.getBirthday());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByBirthdayAfter() {

		try {
			Teacher teacher1 = new Teacher("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), "Laurea triennale in scienze motorie");
	        entityManager.persist(teacher1);
	        entityManager.flush();

	        // when
	        List<Teacher> founds = teacherRepository.findByBirthdayAfter(new SimpleDateFormat("yyyy-MM-dd").parse("1969-12-31"));
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getBirthday()).isEqualTo(teacher1.getBirthday());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByBirthdayBetween() {
    	
		try {
			Teacher teacher1 = new Teacher("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), "Laurea triennale in scienze motorie");
	        entityManager.persist(teacher1);
	        entityManager.flush();

	        // when
	        List<Teacher> founds = teacherRepository.findByBirthdayBetween(new SimpleDateFormat("yyyy-MM-dd").parse("1969-12-31"), new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-02"));
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getBirthday()).isEqualTo(teacher1.getBirthday());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByNameAndSurnameIgnoreCase() {
    	try {
			Teacher teacher1 = new Teacher("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), "Laurea triennale in scienze motorie");
	        entityManager.persist(teacher1);
	        entityManager.flush();

	        // when
	        List<Teacher> founds = teacherRepository.findByNameAndSurnameAllIgnoreCase(teacher1.getName().toUpperCase(), teacher1.getSurname().toUpperCase());
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getName()).isEqualTo(teacher1.getName());
	        assertThat(founds.get(0).getSurname()).isEqualTo(teacher1.getSurname());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenDeleteCourse() {
    	try {
			Teacher teacher1 = new Teacher("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), "Laurea Triennale in scienze motorie");
			Course course = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, null);
			teacher1.getCourses().add(course);
	        entityManager.persist(teacher1);
			entityManager.persist(course);
	        entityManager.flush();

	        // when
	        Optional<Teacher> found = teacherRepository.findByCourses(course);
	     
	        // then
	        assertThat(found.isPresent());
	        assertThat(found.equals(teacher1));

	        entityManager.remove(course);
	        entityManager.flush();

	        // when
	        ArrayList <Teacher> founds =  Lists.newArrayList(teacherRepository.findAll());
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0)).isEqualTo(teacher1);

	        // when
	        found = teacherRepository.findByCourses(course);
	     
	        // then
	        assertThat(!found.isPresent());
	        
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
}