package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.model.Client;
import com.example.demo.model.Course;
import com.example.demo.model.Teacher;
import com.example.demo.repository.CourseRepository;
import com.google.common.collect.Lists;

@RunWith(SpringRunner.class)
@TestPropertySource(locations="classpath:test.properties")
@DataJpaTest
public class CourseRepositoryIntegrationTest {
 
    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private CourseRepository courseRepository;
 
    @Test
    public void whenFindByTeacher() {
    	
		try {
			Teacher teacher1 = new Teacher("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), "Laurea triennale in scienze motorie");
	        entityManager.persist(teacher1);
			Course course = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, teacher1);
			entityManager.persist(course);
	        entityManager.flush();

	        // when
	        List<Course> founds = courseRepository.findByTeacher(teacher1);
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getTeacher()).isEqualTo(teacher1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByStartDateBefore() {
    	
		try {
			Course course = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, null);
			entityManager.persist(course);
	        entityManager.flush();

	        // when
	        List<Course> founds = courseRepository.findByStartDateBefore(new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-02"));
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getStartDate()).isEqualTo(course.getStartDate());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByEndDateAfter() {
    	
		try {
			Course course = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, null);
			entityManager.persist(course);
	        entityManager.flush();

	        // when
	        List<Course> founds = courseRepository.findByStartDateBefore(new SimpleDateFormat("yyyy-MM-dd").parse("2019-09-30"));
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getEndDate()).isEqualTo(course.getEndDate());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByNameIgnoreCase() {
    	
		try {
			Course course = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, null);
			entityManager.persist(course);
	        entityManager.flush();

	        // when
	        List<Course> founds = courseRepository.findByNameIgnoreCase(course.getName());
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getName()).isEqualToIgnoringCase(course.getName());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByClients() {
    	
		try {
			Course course = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, null);
			entityManager.persist(course);
			Client client1 = new Client("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), null, "Italiana");
	        client1.getEnrollments().add(course);
	        entityManager.persist(client1);
	        entityManager.flush();

	        // when
	        List<Course> founds = courseRepository.findByClients(client1);
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0)).isEqualTo(course);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByNameContaining() {
    	
		try {
			Course course = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, null);
			entityManager.persist(course);
	        entityManager.flush();

	        // when
	        List<Course> founds = courseRepository.findByNameContaining(course.getName().substring(2));
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getName()).isEqualTo(course.getName());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByCostLessThanEqualOrderByCostAsc() {
    	
		try {
			Course course1 = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, null);
			entityManager.persist(course1);
			Course course2 = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 230, null);
			entityManager.persist(course2);
	        entityManager.flush();

	        // when
	        List<Course> founds = courseRepository.findByCostLessThanEqualOrderByCostAsc(course2.getCost());
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getCost()).isEqualTo(course1.getCost());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByCostGreaterThanEqualOrderByCostAsc() {
    	
		try {
			Course course1 = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 210, null);
			entityManager.persist(course1);
			Course course2 = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, null);
			entityManager.persist(course2);
	        entityManager.flush();

	        // when
	        List<Course> founds = courseRepository.findByCostGreaterThanEqualOrderByCostAsc(course1.getCost());
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getCost()).isEqualTo(course1.getCost());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenFindByCostBetween() {
    	
		try {
			Course course1 = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 210, null);
			entityManager.persist(course1);
	        entityManager.flush();

	        // when
	        List<Course> founds = courseRepository.findByCostBetween(course1.getCost()-10, course1.getCost()+10);
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0).getCost()).isEqualTo(course1.getCost());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenDeleteClient() {
    	try {
			Course course = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, null);
			Client client1 = new Client("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), null, "Italiana");
	        entityManager.persist(client1);
			entityManager.persist(course);
	        entityManager.flush();
	        
	        client1.getEnrollments().add(course);
	        
	        // when
	        List<Course> founds = courseRepository.findByClients(client1);
	        
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0)).isEqualTo(course);

	        entityManager.remove(client1);
	        entityManager.flush();

	        // when
	        founds =  Lists.newArrayList(courseRepository.findAll());
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0)).isEqualTo(course);

	        // when
	        founds = courseRepository.findByClients(client1);
	     
	        // then
	        assertThat(founds.size()).isZero();
	        
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
 
    @Test
    public void whenDeleteTeacher() {
    	try {
			Teacher teacher1 = new Teacher("Mario", "Rossi", "mario.rossi@mail.com", "+393333333333", "Via dei mille, 1", "Milano", new Date(new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01").getTime()), "Laurea Triennale in scienze motorie");
			Course course = new Course("Yoga", new SimpleDateFormat("yyyy-MM-dd").parse("2019-01-01"), new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01"), 220, teacher1);
	        entityManager.persist(teacher1);
			entityManager.persist(course);
	        entityManager.flush();
	        entityManager.refresh(teacher1);

	        // when
	        List<Course> founds = courseRepository.findByTeacher(teacher1);
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0)).isEqualTo(course);

	        entityManager.remove(teacher1);
	        entityManager.flush();

	        // when
	        founds =  Lists.newArrayList(courseRepository.findAll());
	     
	        // then
	        assertThat(founds.size()).isPositive();
	        assertThat(founds.get(0)).isEqualTo(course);

	        // when
	        founds = courseRepository.findByTeacher(teacher1);
	     
	        // then
	        assertThat(founds.size()).isZero();
	        
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
}