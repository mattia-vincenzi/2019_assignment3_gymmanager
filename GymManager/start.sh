#!/bin/sh

MYSQL_USER=root
MYSQL_PASS=admin
MYSQL_HOST=db
MYSQL_CONN="-u${MYSQL_USER} -p${MYSQL_PASS} -h${MYSQL_HOST}"
mysqladmin ping ${MYSQL_CONN} 2>/dev/null 1>/dev/null
MYSQLD_RUNNING=${?}
while [ ${MYSQLD_RUNNING} -ne 0 ]; do
	echo "I'm waiting for MySql"
    sleep 1
	mysqladmin ping ${MYSQL_CONN} 2>/dev/null 1>/dev/null
	MYSQLD_RUNNING=${?}
done

java -Djava.security.egd=file:/dev/./urandom -jar app.jar